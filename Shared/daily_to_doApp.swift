//
//  daily_to_doApp.swift
//  Shared
//
//  Created by Игорь Дячук on 07.02.2021.
//

import SwiftUI

@main
struct daily_to_doApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
